class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, :storage => :cloudinary, :path => 'muvent/avatar/:filename'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates :first_name, :last_name, presence: true

  has_many :events, dependent: :destroy
  has_many :musics, dependent: :destroy
end
