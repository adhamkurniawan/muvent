class Event < ApplicationRecord
  belongs_to :user

  validates :name, :start_event, :end_event, :location, presence: true
  validates :name, length: {minimum: 5, maximum: 50}
end
