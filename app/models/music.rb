class Music < ApplicationRecord
  has_attached_file :mp3, :storage => :cloudinary, :path => 'muvent/musics/:filename', cloudinary_resource_type: :video
  validates_attachment :mp3, :content_type => { :content_type => ["audio/mpeg", "audio/mp3"] }, :file_name => { :matches => [/mp3\Z/] }

  belongs_to :user

  validates :name, presence: true
end
