class UsersController < ApplicationController
  def index
    @users = User.all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @music = Music.find(params[:id])
    @user = User.find(params[:id])
    @musics = Music.where(user_id: @user).paginate(:page => params[:page], :per_page => 10)
  end
end
