class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_event, except: [:index, :new, :create]

  def index
    @user = current_user
    @events = Event.where(user_id: @user).paginate(:page => params[:page], :per_page => 10)
  end

  def show
  end

  def new
    @event = current_user.events.build
  end

  def create
    @event = current_user.events.build(event_params)

    if @event.save
      flash[:success] = "Success created event."
      redirect_to events_url
    else
      flash[:danger] = "Something went wrong."
      redirect_back(fallback_location: events_url)
    end
  end

  def edit
  end

  def update
    if @event.update(event_params)
      flash[:success] = "Success changed event."
      redirect_to events_url
    else
      flash[:danger] = "Something went wrong."
      redirect_back(fallback_location: events_url)
    end
  end

  def destroy
    @event.destroy
    redirect_to events_url, success: "Event has been deleted."
  end

  private
    def find_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:name, :start_event, :end_event, :location, :info)
    end
end
