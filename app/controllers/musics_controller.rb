class MusicsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_music, only: [:show, :edit, :update, :destroy]

  def index
    @user = current_user
    @musics = Music.where(user_id: @user).paginate(:page => params[:page], :per_page => 10)
  end

  def show
  end

  def new
    @music = current_user.musics.build
  end

  def edit
  end

  def create
    @music = current_user.musics.build(music_params)

    if @music.save
      flash[:success] = "Success added new music."
      redirect_to musics_url
    else
      flash[:danger] = "Something went wrong."
      redirect_back(fallback_location: musics_url)
    end
  end

  def update
    if @music.update(music_params)
      flash[:success] = "Success changed music."
      redirect_to musics_url
    else
      flash[:danger] = "Something went wrong."
      redirect_back(fallback_location: musics_url)
    end
  end

  def destroy
    @music.destroy
    flash[:success] = "Success deleted music."
    redirect_to musics_url
  end

  private
    def set_music
      @music = Music.find(params[:id])
    end

    def music_params
      params.require(:music).permit(:name, :mp3)
    end
end
