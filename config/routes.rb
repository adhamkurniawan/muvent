Rails.application.routes.draw do
  resources :musics
  devise_for :users
  root 'pages#index'

  resources :users, only: [:show, :index]
  resources :events

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
