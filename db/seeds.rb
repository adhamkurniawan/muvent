# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users = User.create([
  {first_name: 'Charlie', last_name: 'Puth', email: 'charlieputh@gmail.com', password: 'password', password_confirmation: 'password'},
  {first_name: 'Shawn', last_name: 'Mendes', email: 'shawn@gmail.com', password: 'password', password_confirmation: 'password'},
  {first_name: 'Taylor', last_name: 'Swift', email: 'swift@gmail.com', password: 'password', password_confirmation: 'password'},
  {first_name: 'Maroon', last_name: '5', email: 'maroon@gmail.com', password: 'password', password_confirmation: 'password'},
])
