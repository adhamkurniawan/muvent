class AddAttachmentMp3ToMusics < ActiveRecord::Migration[5.1]
  def self.up
    change_table :musics do |t|
      t.attachment :mp3
    end
  end

  def self.down
    remove_attachment :musics, :mp3
  end
end
