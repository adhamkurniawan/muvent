class ChangeDateFormatInEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :start_event, :datetime
    add_column :events, :end_event, :datetime
  end
end
